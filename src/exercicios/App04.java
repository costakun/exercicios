package exercicios;

import java.text.DecimalFormat;
import java.util.Scanner;

/*
 * Repeticao e Selecao 13
 * 
 */

public class App04 {

	public static void main(String[] args) {
		
		double altura = 0, maior = 0;
		String nome = "", nome_m = "" ;
		int num = 0;
		
		Scanner tecla = new Scanner(System.in);
		
		DecimalFormat df = new DecimalFormat("#0.00");
		
		try {
			
			while(!nome.equals("FIM")) {

				
				System.out.println("Nome: ");
				nome = tecla.nextLine();
				
				if (nome.equals("FIM") || nome.equals("fim")) {
					
					System.out.println("Total de mocas no concurso: " + num);
					
					System.out.println("Moca mais alta: " + nome_m);
					
					System.out.println("Altura: " + df.format(maior));
					tecla.close();
				} else if(!nome.equals("FIM") || nome.equals("fim"))
				{
					System.out.println("Altura: ");
					altura = tecla.nextDouble();
					
					num++;
					
					if (altura > maior ) {
						maior = altura;
						nome_m = nome;
					}
				}
				
				tecla.nextLine();

			}
			
		}catch (Exception e){
			System.out.println(e);
		}

	}

}
